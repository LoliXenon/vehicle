/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gd2.vehicleinheritance;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Nate
 */
public class vehicleTest
    {
    
    public vehicleTest()
        {
        }
    
    @BeforeClass
    public static void setUpClass()
        {
        }
    
    @AfterClass
    public static void tearDownClass()
        {
        }
    
    @Before
    public void setUp()
        {
        }
    
    @After
    public void tearDown()
        {
        }

    /**
     * Test of steer method, of class vehicle.
     */
    @Test
    public void testSteer()
        {
        System.out.println("steer");
        double direction = 90.0;
        vehicle instance = new vehicle();
        testSteer(instance, direction, 90);
        testSteer(instance, direction, 180);
        direction = -45;
        testSteer(instance, direction, 135);
        }
    
    public void testSteer(vehicle instance, double direction, double expected)
        {
        instance.steer(direction);
        double actualDirection = instance.getCurrentDirection();
        assertEquals(expected, actualDirection, 0.0);
        }

    /**
     * Test of move method, of class vehicle.
     */
    @Test
    public void testMove()
        {
        System.out.println("move");
        double speed = 10.0;
        double direction = 10.0;
        vehicle instance = new vehicle();
        instance.move(speed, direction);
        assertEquals(speed, instance.getCurrentSpeed(), 0.0);
        assertEquals(direction, instance.getCurrentDirection(), 0.0);
        }

    /**
     * Test of getName method, of class vehicle.
     */
    @Ignore
    public void testGetName()
        {
        System.out.println("getName");
        vehicle instance = new vehicle();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
        }

    /**
     * Test of getSize method, of class vehicle.
     */
    @Ignore
    public void testGetSize()
        {
        System.out.println("getSize");
        vehicle instance = new vehicle();
        String expResult = "";
        String result = instance.getSize();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
        }

    /**
     * Test of getSpeed method, of class vehicle.
     */
    @Ignore
    public void testGetSpeed()
        {
        System.out.println("getSpeed");
        vehicle instance = new vehicle();
        double expResult = 0.0;
        double result = instance.getCurrentSpeed();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
        }

    /**
     * Test of getDirection method, of class vehicle.
     */
    @Ignore
    public void testGetDirection()
        {
        System.out.println("getDirection");
        vehicle instance = new vehicle();
        double expResult = 0.0;
        double result = instance.getCurrentDirection();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
        }
    
       @Test
    public void testStop()
        {
        System.out.println("stop");
        double speed = 10.0;
        double direction = 10.0;
        vehicle instance = new vehicle();
        instance.move(speed, direction);
        instance.stop();
        speed = 0.0;
        assertEquals(speed, instance.getCurrentSpeed(), 0.0);
        assertEquals(direction, instance.getCurrentDirection(), 0.0);
        }

    }
