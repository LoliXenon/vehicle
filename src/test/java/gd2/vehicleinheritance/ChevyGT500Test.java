/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gd2.vehicleinheritance;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nate
 */
public class ChevyGT500Test
    {
    
    public ChevyGT500Test()
        {
        }
    
    @BeforeClass
    public static void setUpClass()
        {
        }
    
    @AfterClass
    public static void tearDownClass()
        {
        }
    
    @Before
    public void setUp()
        {
        }
    
    @After
    public void tearDown()
        {
        }

    /**
     * Test of accelerate method, of class ChevyGT500.
     */
    @Test
    public void testAccelerate()
        {
        System.out.println("accelerate");
        double rate = 0.0;
        ChevyGT500 instance = new ChevyGT500();
        accelerateCheckSpeedandGear(instance, 0, 0.0, 'N');
        accelerateCheckSpeedandGear(instance, 1, 1.0, '1');
        accelerateCheckSpeedandGear(instance, 39, 40.0, '1');
        accelerateCheckSpeedandGear(instance, 1, 41.0, '2');
        accelerateCheckSpeedandGear(instance, 29, 70.0, '2');
        accelerateCheckSpeedandGear(instance, 1, 71.0, '3');
        accelerateCheckSpeedandGear(instance, 29, 100.0, '3');
        accelerateCheckSpeedandGear(instance, 1, 101.0, '4');
        accelerateCheckSpeedandGear(instance, 29, 130.0, '4');
        accelerateCheckSpeedandGear(instance, 1, 131.0, '5');
        accelerateCheckSpeedandGear(instance, 29, 160.0, '5');
        accelerateCheckSpeedandGear(instance, 1, 161.0, '6');
        accelerateCheckSpeedandGear(instance, 41, 202.0, '6');
        accelerateCheckSpeedandGear(instance, 41, 202.0, '6');
        accelerateCheckSpeedandGear(instance, -202, 0.0, 'N');
        }
    
    public void accelerateCheckSpeedandGear(ChevyGT500 instance, double rate, double expectedSpeed, char expectedGear)
    {
    instance.accelerate(rate);
    double currentSpeed = instance.getCurrentSpeed();
    assertEquals(expectedSpeed, currentSpeed, 0.0);
    char gear = instance.getCurrentGear();
    assertEquals(expectedGear, gear);
    }
    
    }
