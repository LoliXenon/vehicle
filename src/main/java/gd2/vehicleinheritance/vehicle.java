/*
 * Challenge.
 * Start with base class of a vehicle, then create a car class that inherits from vehicle
 * the create another class that inherits from car class.
 * Should be able to handle steering, changing gears, moving (speed, rotation), accel/decelerate
 * Decide where methods and behaviours go
 * For your specific type of car, implement something specific
 */

package gd2.vehicleinheritance;

/**
 *
 * @author Nate
 */
public class vehicle
    {
    private String name;
    private String size;
    private double currentSpeed;
    private double currentDirection;
    private double maxSpeed;
    
    public vehicle()
        {
        this.name = "NoName";
        this.size = "NoSize";
        this.maxSpeed = 0.0;
        
        this.currentSpeed = 0.0;
        this.currentDirection = 0.0;
        }
    
    public void steer(double direction)
        {
        this.currentDirection += direction;
        System.out.println("Vehicle.steer(): Steering at " + currentDirection + " degrees");
        }
    
    public void move(double speed, double direction)
        {
        currentSpeed = speed;
        currentDirection = direction;
        System.out.println("Vehicle.move(): Vehicle is moving at " + currentSpeed + " in the direction " + currentDirection);
        }

    public vehicle(String name, String size, double maxSpeed)
        {
        this.name = name;
        this.size = size;
        this.maxSpeed = maxSpeed;
        
        this.currentSpeed = 0.0;
        this.currentDirection = 0.0;
        }
    
    public void stop()
        {
        this.currentSpeed = 0;
        }
    
    

    public String getName()
        {
        return name;
        }

    public String getSize()
        {
        return size;
        }

    public double getCurrentSpeed()
        {
        return currentSpeed;
        }

    public double getCurrentDirection()
        {
        return currentDirection;
        }
    
    public double getMaxSpeed()
        {
        return maxSpeed;
        }

    
    
    }
