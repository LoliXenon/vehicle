/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gd2.vehicleinheritance;

/**
 *
 * @author Nate
 */
public class car extends vehicle
    {
    private int gears;
    private int wheels;
    private int doors;
    private boolean isManual;
    
    private char currentGear;
    
    

    public car(String name, String size, double maxSpeed, int gears, int wheels, int doors, boolean isManual)
        {
        super(name, size, maxSpeed);
        this.gears = gears;
        this.wheels = wheels;
        this.doors = doors;
        this.isManual = isManual;
        this.currentGear = 'N';
        }
    
    public car()
        {
        this("NoName", "NoSize", 0.0, 0, 0, 0, false);
        }

    public void changeGear(char currentGear)
        {
        this.currentGear = currentGear;
        System.out.println("Car.setCurrentGear(): Changed to " + this.currentGear + " gear");
        }
    
    public void changeVelocity(double speed, double direction)
        {
        System.out.println("Car.changeVelocity: Speed " + speed + "direction " + direction);
        move(speed, direction);
        
        }

    public char getCurrentGear()
        {
        return currentGear;
        }
    
    
    
    
    }
